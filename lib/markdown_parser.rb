require 'redcarpet'

class MarkdownParser
  PARSER = Redcarpet::Markdown.new(Redcarpet::Render::HTML)

  def self.render(md)
    PARSER.render(md)
  end
end
