class SitePermissionController < BaseController
  before_action :ensure_site_access, except: [:new, :create]

  def ensure_site_access
    site_id = params[:site_id] || params[:id]
    accessible = current_user.can_access_site?(site_id)
    er_message = "You don't have access to this site."
    flash_and_redirect(root_path, danger: er_message) unless accessible
  end
end
