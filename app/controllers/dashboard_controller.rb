class DashboardController < BaseController
  def show
    @sites = current_user.sites
  end

end
