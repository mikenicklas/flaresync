class VersionsController < SitePermissionController
  def new
    @region = EditableRegion.find(params[:editable_region_id])
  end

  def create
    region = EditableRegion.find(params['editable_region_id'])
    if Version.create_new_version?(region: region, params: params)
      flash_and_redirect site_editable_region_index_path(region.site_id),
        success: 'Your new version has been saved successfully!'
    else
      render :new
    end
  end
end
