class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def flash_and_redirect(route, flash_messages={})
    flash_messages.each do |type, content|
      flash[type] = content
    end
    redirect_to route
  end

  def flash_and_render(route, flash_messages={})
    flash_messages.each do |type, content|
      flash[type] = content
    end
    render route
  end
end
