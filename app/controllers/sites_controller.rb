class SitesController < SitePermissionController
  def new
    @site = Site.new(user_id: current_user.id)
  end

  def create
    @site = Site.new(site_params.merge!(user_id: current_user.id))
    if @site.save
      flash_and_redirect(site_editable_region_index_path(@site.id), success: 'Your site has been successfully created!')
    else
      flash_and_render(:new, danger: 'Sorry, your site could not be created.')
    end
  end

  def show
    @site = Site.find(params[:id])
  end

  private

  def site_params
    params.require(:site).permit(:name, :url)
  end

end
