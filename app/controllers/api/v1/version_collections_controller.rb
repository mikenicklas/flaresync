class Api::V1::VersionCollectionsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    @collection = VersionCollection.new(params[:regions])
    render :json => @collection.json, :callback => params[:callback]
  end
end
