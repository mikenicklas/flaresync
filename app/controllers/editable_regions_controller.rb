class EditableRegionsController < SitePermissionController
  before_action :set_site

  def index
  end

  def new
    @region = @site.editable_regions.new
  end

  def create
    @erv = EditableRegionVersion.new(user: current_user, params: params)
    if @erv.create_region_and_version
      flash_and_redirect(site_editable_region_index_path(params[:site_id]), success: 'Your region has been created!')
    else
      flash_and_render(:new, danger: 'Sorry, your editable region could not be created.')
    end
  end

  def show
    @region = EditableRegion.find(params[:id])
  end

  private

  def set_site
    @site = current_user.sites.find(params[:site_id])
  end
end
