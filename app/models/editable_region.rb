class EditableRegion < ActiveRecord::Base
  belongs_to :site
  has_many :versions

  validates :name, presence: true, uniqueness: {:scope => :site_id}

  default_scope { order(:name => :asc) }

  enum region_type: [:basic, :specific]

  def latest_version
    versions = self.versions
    versions.any? ? versions.last : nil
  end
end
