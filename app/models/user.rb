class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :sites

  def can_access_site?(site_or_id)
    site = site_or_id.respond_to?(:id) ? site_or_id : Site.find_by_id(site_or_id.to_i)
    return false unless self.sites.any?
    self.sites.include?(site)
  end

  def admin?
    self.admin == true
  end
end
