class Site < ActiveRecord::Base
  belongs_to :user
  has_many :editable_regions

  validates :name, presence: true, uniqueness: {:scope => :user_id}
  validates :url, presence: true, uniqueness: {:scope => :user_id}

  default_scope { order(:name => :asc) }

  def regions
    self.editable_regions
  end

  def read_script
    if reader = Scripting::Reader.new(self)
      reader.js
    end
  end
end
