# VersionCollection is a helper non-activerecord model to collect the latest
# versions for explicitly defined regions IDs separated by colons.
#
#
# Example Usage:
#
# region_ids = '1:2'
# collection = VersionCollection.new(region_ids)
#
# collection.get_version_collection
# => {region_1: { content: version_1.html }
# => {region_2: { content: version_2.html } }
#
#
# collection.json
# => ( returns JSON version of #get_version_collection )
#

require 'json'

class VersionCollection
  def initialize(region_ids)
    @region_ids = region_ids.split(':')
  end

  def versions
    latest_versions = @region_ids.map {|id| EditableRegion.find(id.to_i).latest_version }
    latest_versions.delete_if {|i| i.nil?}
    latest_versions
  end

  def json
    content = get_version_content
    content.to_json
  end

  def get_version_content
    content = {}
    versions.each do |v|
      content["region_#{v.editable_region_id}"] = {content: v.html}
    end
    content
  end

end
