require 'uglifier'

module Scripting
  class Reader
    BASE_URL = ENV['ROOT_DOMAIN'] + '/api/v1/version_collections/'

    def initialize(site)
      @site = site
      @regions = site.editable_regions
    end

    def js
      full_script(site_url_string)
    end

    def site_url_string
      region_string = site_regions.join(':')
      BASE_URL + "?site=#{@site.id}&regions=#{region_string}"
    end

    def site_regions
      regions = @regions.select {|r| r.versions.any?}
      regions.map do |region|
        region.id
      end
    end

    def full_script(url)
      script = <<-FULL
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js"></script><script>#{Uglifier.compile(base_script(url))}</script>
      FULL
      script.strip
    end

    def base_script(url)
      <<-SOURCE
      $(document).ready(function() {
        $.ajax({
          url: "#{url}",
          dataType: "jsonp",
        }).success(function(json) {
          var regions = $('[data-region]');
          regions.each(function() {
            var region = $(this).data('region');
            if(json[region] && json[region].content) {
              $(this).html(unescape(json[region].content))
            }
          });
        });
      });
      SOURCE
    end

  end
end
