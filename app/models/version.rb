class Version < ActiveRecord::Base
  belongs_to :editable_region
  validates :content, presence: true

  def to_html
    MarkdownParser.render(self.content)
  end

  def self.create_new_version?(region:, params:)
    latest_version = region.latest_version
    if latest_version.content != params['content']
      region.versions.create(content: params['content'])
    end
    true
  end

  def convert_and_save
    html = to_html
    self.html_content = html
    self.converted = true
    self.save
    html
  end

  def html
    self.converted ? self.html_content : self.convert_and_save
  end

end
