class EditableRegionVersion

  def initialize(user:, params:)
    @user = user
    @params = params
  end

  def create_region_and_version
    return false unless valid?
    if region = create_region
      version = create_version_and_convert
    end
    region && version
  end

  def find_site_by_id
    @site = @user.sites.find_by_id(@params[:site_id])
  end

  def create_version_and_convert
    if version = @region.versions.create(content: @params['content'])
      MarkdownConverterWorker.perform_async(version.id)
      return true
    end
    false
  end

  def create_region
    name, type = @params['name'], @params['region_type']
    @region = @site.editable_regions.create(name: name, region_type: type)
  end

  def valid?
    @user && @params && find_site_by_id
  end

end
