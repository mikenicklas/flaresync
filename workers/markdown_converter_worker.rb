require 'sidekiq'

class MarkdownConverterWorker
  include Sidekiq::Worker

  def perform(version_id)
    if version = Version.find(version_id)
      html = MarkdownParser.render(version.content)
      version.html_content = html
      version.converted = true
      version.save
    end
  end
end
