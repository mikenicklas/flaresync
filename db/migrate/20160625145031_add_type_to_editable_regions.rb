class AddTypeToEditableRegions < ActiveRecord::Migration
  def change
    add_column :editable_regions, :type, :integer
  end
end
