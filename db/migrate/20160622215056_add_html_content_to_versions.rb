class AddHtmlContentToVersions < ActiveRecord::Migration
  def change
    add_column :versions, :html_content, :text
  end
end
