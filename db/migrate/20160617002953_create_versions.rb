class CreateVersions < ActiveRecord::Migration
  def change
    create_table :versions do |t|
      t.text :content
      t.integer :editable_region_id

      t.timestamps null: false
    end
  end
end
