class AddConvertedToVersions < ActiveRecord::Migration
  def change
    add_column :versions, :converted, :boolean, default: false
  end
end
