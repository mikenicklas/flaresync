class AddDefaultValueToRegionType < ActiveRecord::Migration
  def up
    change_column :editable_regions, :region_type, :integer, :default => 0
  end

  def down
    change_column :editable_regions, :region_type, :integer, :default => nil
  end
end
