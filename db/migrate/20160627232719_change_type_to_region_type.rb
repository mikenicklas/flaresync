class ChangeTypeToRegionType < ActiveRecord::Migration
  def change
    rename_column :editable_regions, :type, :region_type
  end
end
