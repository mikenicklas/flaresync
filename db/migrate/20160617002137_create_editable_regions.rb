class CreateEditableRegions < ActiveRecord::Migration
  def change
    create_table :editable_regions do |t|
      t.string :name
      t.integer :site_id

      t.timestamps null: false
    end
  end
end
