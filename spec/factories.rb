FactoryGirl.define do
  factory :user do
    email 'test@example.com'
    password 'testing123'
    password_confirmation 'testing123'
  end

  factory :site do
    name 'Test Site'
    url 'http://testsite.com'
  end

  factory :editable_region do
    name 'Hero Section'
  end

  factory :version do
    content  '# Welcome!'
  end

end
