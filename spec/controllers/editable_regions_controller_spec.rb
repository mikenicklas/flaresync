require 'rails_helper'

describe EditableRegionsController do

  let(:params) do
    { 'name' => 'A New Region',
      'content' => '# Some markdown content' }
  end
  let(:user) { create(:user) }
  let(:site) { create(:site) }

  context 'success' do
    it 'creates a new region' do
      sign_in user
      site.user_id = user.id
      site.save
      params['site_id'] = site.id
      post :create, params
      expect(site.regions.size).to eql 1
    end

    it 'creates a new region version' do
      sign_in user
      site.user_id = user.id
      site.save
      params['site_id'] = site.id
      post :create, params
      expect(site.regions.first.versions.size).to eql 1
    end

    it 'queues markdown to html conversion' do
      sign_in user
      site.user_id = user.id
      site.save
      params['site_id'] = site.id
      expect(MarkdownConverterWorker).to receive(:perform_async)
      post :create, params
    end

    it 'redirects to version root with flash' do
      sign_in user
      site.user_id = user.id
      site.save
      params['site_id'] = site.id
      post :create, params
      expect(controller).to redirect_to site_editable_region_index_path(site.id)
      expect(flash[:success]).to be_truthy
    end

    it 'sets #region_type' do
      sign_in user
      site.user_id = user.id
      site.save
      params['region_type'] = :specific
      params['site_id'] = site.id
      post :create, params
      expect(user.sites.last.editable_regions.last.region_type).to eql 'specific'
    end
  end

  context 'failure' do
    it 'renders new with flash' do
      sign_in user
      site.user_id = user.id
      site.save
      params['site_id'] = site.id
      allow_any_instance_of(EditableRegionVersion).to receive(:create_region_and_version).and_return false
      post :create, params
      expect(controller).to render_template :new
      expect(flash[:danger]).to be_truthy
    end
  end

end
