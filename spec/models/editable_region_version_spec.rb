require 'rails_helper'
require 'pry'

describe EditableRegionVersion do
  subject { EditableRegionVersion }
  let(:user) { create(:user, id: 1) }
  let(:site) { create(:site, id: 1, user_id: 1) }
  let(:params) do
    { 'name' => 'Some Region Name',
      'site_id' => 1,
      'content' => '# Some Markdown',
      'region_type' => :specific }
  end

  describe '#new' do
    it 'sets @user and @params' do
      allow_any_instance_of(subject).to receive(:find_site_by_id).and_return site
      erv = subject.new(user: user, params: params)
      ['params', 'user'].each do |instance_var|
        value = erv.instance_variable_get("@#{instance_var}")
        expect(value).to be_truthy
      end
    end
  end

  describe '#valid?' do
    it 'returns false unless @site, @user, and @params' do
      erv = subject.new(user: user, params: params)
      ['params', 'user', 'site'].each do |instance_var|
        erv.instance_variable_set("@#{instance_var}", nil)
        expect(erv.valid?).to be_falsey
      end
    end
  end

  describe '#create_region_and_version' do
    it 'returns immediately if invalid' do
      erv = subject.new(user: user, params: params)
      allow(erv).to receive(:valid?).and_return false
      expect(erv.create_region_and_version).to eql false
    end

    it 'creates regions and versions' do
      erv = subject.new(user: user, params: params)
      erv.instance_variable_set('@site', site)
      allow(erv).to receive(:valid?).and_return true
      expect(erv.create_region_and_version).to be_truthy
      regions = user.sites.last.editable_regions
      expect(regions.last.name).to eql params['name']
      expect(regions.last.versions.last.content).to eql params['content']
    end

    it 'returns false if region creation fails' do
      erv = subject.new(user: user, params: params)
      allow(erv).to receive(:create_region).and_return false
      allow(erv).to receive(:create_version_and_convert).and_return true
      allow(erv).to receive(:valid?).and_return true
      expect(erv.create_region_and_version).to eql false
    end

    it 'returns false if version creation fails' do
      erv = subject.new(user: user, params: params)
      allow(erv).to receive(:create_region).and_return true
      allow(erv).to receive(:create_version_and_convert).and_return false
      allow(erv).to receive(:valid?).and_return true
      expect(erv.create_region_and_version).to eql false
    end
  end

  describe '#create_region' do
    it 'success: creates a new region' do
      expect(site.editable_regions.size).to eql 0
      erv = subject.new(user: user, params: params)
      erv.instance_variable_set('@site', site)
      erv.create_region
      expect(site.editable_regions.size).to eql 1
    end

    it 'failure: returns false' do
      erv = subject.new(user: user, params: params)
      erv.instance_variable_set('@site', site)
      allow_any_instance_of(Site).to receive_message_chain('editable_regions.create').and_return false
      expect(erv.create_region).to be_falsey
    end
  end

  describe '#create_version_and_convert' do
    it 'success: creates a region' do
      erv = subject.new(user: user, params: params)
      erv.instance_variable_set('@site', site)
      erv.instance_variable_set('@region', create(:editable_region, site_id: site.id))
      erv.create_version_and_convert
      expect(erv.instance_variable_get('@region').versions.last.content).to eql params['content']
    end

    it 'success: queues conversion to HTML' do
      expect(MarkdownConverterWorker).to receive(:perform_async)
      erv = subject.new(user: user, params: params)
      erv.instance_variable_set('@site', site)
      erv.instance_variable_set('@region', create(:editable_region, site_id: site.id))
      allow_any_instance_of(EditableRegion).to receive_message_chain('versions.create').and_return create(:version)
      erv.create_version_and_convert
    end

    it 'failure: returns false' do
      erv = subject.new(user: user, params: params)
      erv.instance_variable_set('@site', site)
      erv.instance_variable_set('@region', create(:editable_region, site_id: site.id))
      allow_any_instance_of(EditableRegion).to receive_message_chain('versions.create').and_return false
      expect(erv.create_version_and_convert).to eql false
    end
  end

end
