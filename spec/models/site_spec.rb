require 'rails_helper'
require 'pry'

RSpec.describe Site, type: :model do
  subject { create(:site) }
  let(:user) { create(:user) }
  let(:user_two) { create(:user, email: 'something@xyz.com', id: '2352345') }
  let(:er) { create(:editable_region) }

  it 'has a unique name scoped to user' do
    user.sites << subject
    dup_site = user.sites.new(name: subject.name, url: subject.url + '.co')
    expect(dup_site).to be_invalid
    non_dup_site = user_two.sites.new(name: subject.name, url: subject.url + '.co')
    expect(non_dup_site).to be_valid
  end

  it 'has a unique url scoped to user' do
    user.sites << subject
    dup_site = user.sites.new(name: subject.name + '!',  url: subject.url)
    expect(dup_site).to be_invalid
    non_dup_site = user_two.sites.new(name: subject.name + '!', url: subject.url)
    expect(non_dup_site).to be_valid
  end

  it 'is invalid without name or url' do
    [:name, :url].each do |attr|
      subject.send("#{attr}=", nil)
      expect(subject).to be_invalid
    end
  end

  it 'has many editable regions' do
    subject.editable_regions << er
    expect(subject.editable_regions.size).to eql 1
  end

  it 'generates read script' do
    allow_any_instance_of(Scripting::Reader).to receive(:js).and_return true
    expect(Scripting::Reader).to receive(:new).with subject
    subject.read_script
  end

  it 'default scope order is name asc' do
    user.sites.create(name: 'Z new Site', url: 'http://lkjfd.com')
    user.sites.create(name: 'A new Site', url: 'http://fad.com')
    expect(user.sites.first.name).to eql 'A new Site'
  end
end
