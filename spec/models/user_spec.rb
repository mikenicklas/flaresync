require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { create(:user) }
  let(:site) { create(:site)}

  it 'has many sites' do
    user.sites << site
    expect(user.sites.size).to eql 1
  end

  context '#can_access_site?' do
    it 'returns true' do
      user.sites << site
      expect(user.can_access_site?(site)).to eql true
    end

    it 'returns false' do
      expect(user.can_access_site?(site)).to eql false
    end

    it 'accepts site instance or id' do
      expect(user.can_access_site?(53)).to eql false
    end
  end

  context '#admin?' do
    it 'returns true for admins' do
      admin = create(:user, admin: true)
      expect(admin.admin?).to eql true
    end

    it 'returns false for admins' do
      non_admin = create(:user, admin: false)
      expect(non_admin.admin?).to eql false
    end
  end
end
