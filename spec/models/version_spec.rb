require 'rails_helper'

RSpec.describe Version, type: :model do
  subject { create(:version) }

  it 'is invalid without content' do
    subject.content = nil
    expect(subject).to be_invalid
  end

  it '#to_html' do
    expect(MarkdownParser).to receive(:render)
    subject.to_html
  end
end
