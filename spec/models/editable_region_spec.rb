require 'rails_helper'

RSpec.describe EditableRegion, type: :model do
  let(:site) { create(:site) }
  let(:site_two) { create(:site, name: 'Test', url: 'http://testing.com') }
  subject { create(:editable_region) }

  it 'has a unique name scoped to site_id' do
    site.editable_regions << subject
    dup = site.editable_regions.new(name: subject.name)
    expect(dup).to be_invalid
    non_dup = site_two.editable_regions.new(name: subject.name)
    expect(non_dup).to be_valid
  end

  it 'is invalid without a name' do
    subject.name = nil
    expect(subject).to be_invalid
  end

  it 'has many versions' do
    subject.versions << create(:version)
    expect(subject.versions.size).to eql 1
  end

  it '#latest_version' do
    version = create(:version, editable_region_id: subject.id)
    version_two = create(:version, editable_region_id: subject.id)
    expect(subject.latest_version.id).to eql version_two.id
  end

  describe 'type' do
    it '#type' do
      [:basic, :specific].each do |type|
        subject.region_type = type
        subject.save
        expect(subject.region_type).to eql type.to_s
      end
    end

    it '.types' do
      types = EditableRegion.region_types
      expect(types.size).to eql 2
    end

    it 'defaults to basic but can be overridden' do
      expect(subject.region_type).to eql 'basic'
      subject.update(:region_type => :specific)
      expect(subject.region_type).to eql 'specific'
    end
  end

  it 'default scope is name' do
    site.editable_regions.create(name: 'Z new Region')
    site.editable_regions.create(name: 'A new Region')
    expect(site.editable_regions.first.name).to eql 'A new Region'
  end
end
