require 'rails_helper'

describe Scripting::Reader do
  subject { Scripting::Reader }
  let(:site) { create(:site) }
  let(:hero_er) { create(:editable_region, name: 'Hero') }
  let(:footer_er) { create(:editable_region, name: 'Footer') }

  describe '#editable_regions' do
    it 'creates array of latest version ids' do
      allow(hero_er).to receive(:versions).and_return [{id: 1}, {id: 2}]
      allow(footer_er).to receive(:versions).and_return [{id: 4, id: 53}]

      site.editable_regions << hero_er
      site.editable_regions << footer_er

      reader = subject.new(site)
      expect(reader.js.include?("&regions=#{hero_er.id}:#{footer_er.id}")).to eql true
    end
  end

end
