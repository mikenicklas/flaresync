require 'rails_helper'

RSpec.feature "BaseDashboards", type: :feature do
  let(:user) { create(:user) }

  it 'redirects to sign in path if no current user' do
    visit root_path
    expect(page).to have_content 'Log in'
  end

  it 'displays dashboard to signed in users' do
    allow_any_instance_of(ApplicationController).to receive(:current_user).and_return user
    allow_any_instance_of(ApplicationController).to receive(:authenticate_user!).and_return user
    visit root_path
    click_link 'New Site'
    fill_in 'site[name]', with: 'Test Site'
    fill_in 'site[url]', with: 'http://testing.com'
    click_on 'Create Site'
    expect(page).to have_content 'successfully'
    expect(user.sites.size).to eql 1
  end

  it 'only correct user can view their sites' do
    site = create(:site, id: 12)
    allow_any_instance_of(ApplicationController).to receive(:current_user).and_return user
    allow_any_instance_of(ApplicationController).to receive(:authenticate_user!).and_return user
    visit site_path(site.id)
    expect(page).to have_content "You don't have access"
    user.sites << site
    visit site_path(site.id)
    expect(page).to have_content site.name
  end
end
