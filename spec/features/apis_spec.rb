require 'rails_helper'
require 'json'

RSpec.feature "Apis", type: :feature do
  let(:site) { create(:site, id: 20) }
  let(:footer) { create(:version, editable_region_id: 4, content: '<p>Copyright</p>') }
  let(:hero) { create(:version, editable_region_id: 20, content: '<h1>Hello World!</h1>') }

  describe 'versions' do
    it 'returns editable region version content' do
      allow_any_instance_of(VersionCollection).to receive(:versions).and_return [hero, footer]
      visit '/api/v1/version_collections/?site=20&regions=4:20'
      res = JSON.parse(page.body)
      expect(res['region_20']['content'].strip).to eql '<h1>Hello World!</h1>'
      expect(res['region_4']['content'].strip).to eql '<p>Copyright</p>'
    end
  end
end
