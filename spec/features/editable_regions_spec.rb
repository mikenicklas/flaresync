require 'rails_helper'

RSpec.feature "EditableRegions", type: :feature do
  let(:user) { create(:user) }
  let(:site) { create(:site, id: 1) }
  let(:er_one) { create(:editable_region, id: 1, name: 'hero', site_id: 1) }
  let(:er_two) { create(:editable_region, id: 2, name: 'footer', site_id: 1) }

  it '#index' do
    user.sites << site
    site.editable_regions << er_one
    site.editable_regions << er_two
    allow_any_instance_of(ApplicationController).to receive(:current_user).and_return user
    allow_any_instance_of(ApplicationController).to receive(:authenticate_user!).and_return user
    visit '/sites/1/editable_regions'
    expect(page).to have_content 'hero'
    expect(page).to have_content 'footer'
  end

  it '#new' do
    user.sites << site
    allow_any_instance_of(ApplicationController).to receive(:current_user).and_return user
    allow_any_instance_of(ApplicationController).to receive(:authenticate_user!).and_return user
    visit '/sites/1/editable_regions/new'
    fill_in 'name', with: 'A Specific Region'
    fill_in 'content', with: '# Some markdown'
    select 'Specific', from: '_region_type'
    click_on 'Create Editable Region'
    expect(page).to have_content 'A Specific Region'
    region = user.sites.last.editable_regions.last
    expect(region.region_type).to eql 'specific'
  end

  it '#show' do
    user.sites << site
    allow_any_instance_of(ApplicationController).to receive(:current_user).and_return user
    allow_any_instance_of(ApplicationController).to receive(:authenticate_user!).and_return user
    er_one.region_type = :specific
    er_one.save
    version = create(:version, editable_region_id: er_one.id)
    er_one.versions << version
    site.editable_regions << er_one
    visit "/sites/1/editable_regions/#{er_one.id}"
    expect(page).to have_content 'Specific'
    expect(page).to have_text 'Welcome!'
  end
end
