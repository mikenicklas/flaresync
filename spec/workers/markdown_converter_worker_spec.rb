require 'rails_helper'
require 'sidekiq/testing'
Sidekiq::Testing.inline!

describe MarkdownConverterWorker do
  subject { MarkdownConverterWorker }
  let(:version) { create(:version) }

  it 'parses html and updates version' do
    expect(MarkdownParser).to receive(:render)
    expect_any_instance_of(Version).to receive(:save)
    subject.new.perform(version.id)
  end
end
