require 'rails_helper'

describe MarkdownParser do
  let(:content) { create(:version).content }

  it 'converts markdown to html' do
    html = MarkdownParser.render(content)
    expect(html.strip).to eql '<h1>Welcome!</h1>'
  end
end
