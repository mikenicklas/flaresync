require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :users

  root 'dashboard#show'
  get '/test' => 'test#show'

  resources :sites, only: [:new, :create, :show] do
    resources :editable_regions, as: :editable_region do
      resources :versions, only: [:show, :new, :create]
    end
  end

  namespace :api do
    namespace :v1 do
      resources :version_collections, only: [:index]
    end
  end

  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end
end
